<?php

/**
*
*/
require_once('models/Product.php');

class ProductController
{

    function __construct()
    {
        #code...
    }

    public function index()
    {
        $products = Product::all();
        require('views/product/index.php');
    }

    public function create()
    {
        $types = Type::all();
        require('views/product/create.php');
    }

    public function store()
    {
        $product = new Product;
        $product->id = $_POST['id'];
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = DateTime::createFromFormat('d/m/Y', $_POST['fecha']);
        $product->id_tipo = $_POST['id_tipo'];
        $product->store();
        header('location: /product/index');
    }

    public function edit($id)
    {
        $types = Type::all();
        $product = Product::find($id);
        include 'views/product/edit.php';
    }

    public function update($id)
    {
        //buscar registro
        $product = Product::find($id);
        //actualizar campos
        $product->id = $id;
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = $_POST['fecha'];
        $product->id_tipo = $_POST['id_tipo'];
        //ejecutar UPDATE
        $product->save();
        //redireccionar
        header('location: /product/index');
    }

    public function visitar($id)
    {
        $product = Product::find($id);

        if ($product) {
            $_SESSION['products'][] = $product->nombre;
            header('Location: /home');
        } else {
            header('Location: /product/index');
        }
    }
}
