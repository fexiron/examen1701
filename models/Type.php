<?php

/**
*
*/
require('app/Model.php');

class Type extends Model
{
    public $id;
    public $nombre;

    function __construct()
    {
        # code...
    }


    public static function all()/*type*/
    {
        $db = Type::connect();

        $stmt = $db->prepare("SELECT * FROM tipo");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Type');

        $results = $stmt->fetchAll();
        // var_dump($results);
        return $results;
    }

    //hace el INSERT
    public function store()
    {
        $db = $this->connect();
        $sql = "INSERT INTO authors(name, surname, birthdate) VALUES(?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->surname);
        $stmt->bindParam(3, $this->birthdate->format('Y-m-d'));
        $result = $stmt->execute();
        return $result;
    }

    public function find($id)
    {
        $db = Type::connect();
        $sql = "SELECT * FROM tipo WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Type');
        $result = $stmt->fetch();
        return $result;
    }

    public function save()
    {
        $db = $this->connect();
        $sql = "UPDATE authors SET name=?, surname=?, birthdate=? WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->surname);
        $stmt->bindParam(3, $this->birthdate);
        $stmt->bindParam(4, $this->id);
        $result = $stmt->execute();
        return $result;
    }

    public function delete()
    {
        $db = $this->connect();
        $sql = "DELETE FROM authors WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->id);
        $result = $stmt->execute();
        return $result;
    }
}
