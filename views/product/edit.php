<?php require 'views/header.php'; ?>
<main>

    <h1>Edición de producto</h1>

    <form method="post" action="/product/update/<?php echo $product->id; ?>">
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $product->nombre ?>">
        <br>

        <label>Precio</label>
        <input type="text" name="precio" value="<?php echo $product->precio ?>">
        <br>

        <label>Fecha</label>
        <input type="text" name="fecha" value="<?php echo $product->fecha ?>">
        <br>

        <label>Tipo</label>
        <select name="id_tipo">
            <?php foreach ($types as $type): ?>
                <option value="<?php echo $type->id; ?>"> <?php echo $type->nombre; ?></option>
            <?php endforeach ?>
        </select>
        <br>

        <input type="submit" value="Guardar cambios">
        <br>

    </form>
</main>
<?php require 'views/footer.php'; ?>

