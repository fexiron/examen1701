<?php require 'views/header.php'; ?>
<main>
    <div>

        <h1>Lista de productos</h1>

        <table>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Precio </th>
                <th>Fecha</th>
                <th>Tipo</th>
                <th>Acciones</th>
            </tr>
            <?php foreach ($products as $product): ?>
                <?php $type = Type::find($product->id_tipo) ?>
                <tr>
                    <td><?php echo $product->id ?></td>
                    <td><?php echo $product->nombre ?></td>
                    <td><?php echo $product->precio ?></td>
                    <td><?php echo DateTime::createFromFormat('Y-m-d H:m:s', $product->fecha)->format('d-m-Y') ?></td>
                    <td><?php echo $type->nombre ?></td>

                    <td>
                        <a href="<?php echo "/product/visitar/$product->id"?>">visitar</a>
                        -
                        <a href="<?php echo "/product/edit/$product->id"?>">editar</a>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>

        <p>
        <a href="/product/create">Nuevo</a>
        </p>
    </div>

</main>
<?php require 'views/footer.php'; ?>
