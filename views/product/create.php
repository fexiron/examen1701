<?php require 'views/header.php'; ?>
    <main>
        <h1>Alta de Producto</h1>

        <form method="post" action="/product/store">
            <label>Id</label>
            <input type="text" name="id">
            <br>

            <label>Nombre</label>
            <input type="text" name="nombre">
            <br>

            <label>Precio</label>
            <input type="text" name="precio">
            <br>

            <label>Fecha</label>
            <input type="text" name="fecha">
            <br>

            <label>Tipo</label>
            <select name="id_tipo">
                <?php foreach ($types as $type): ?>
                    <option value="<?php echo $type->id; ?>"> <?php echo $type->nombre; ?></option>
                <?php endforeach ?>
            </select>
            <br>

            <input type="submit" value="Nuevo">
            <br>

        </form>
    </main>
<?php require 'views/footer.php'; ?>
